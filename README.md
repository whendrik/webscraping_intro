# Web Scraping Intro

http://bit.ly/webscrapeintro

## Tools

### `Selenium` - Automate Chrome/Firefox through Python

> Selenium automates browsers. That's it! What you do with that power is entirely up to you. 

https://hub.docker.com/u/selenium/

### `BeautifulSoup` - Parse HTML/XML

> Beautiful Soup is a Python library for pulling data out of HTML and XML files. 

https://pypi.org/project/beautifulsoup4/

---

### Prerequisite - Setup Selenium & Python

#### Install Docker

https://store.docker.com/search?type=edition&offering=community

#### Pull the Selenium Docker Image

We use the Standalone Chrome 

https://hub.docker.com/r/selenium/standalone-chrome/

**Pull the Docker Image** with 
```
docker pull selenium/standalone-chrome
```

#### Python - Install the necessary Python Packages

- https://pypi.org/project/selenium/
- https://pypi.org/project/bs4/
```
pip install selenium
pip install bs4
```

---


### Challenge 1 - Start the Engine & Load `https://edition.cnn.com/`

Run the `selenium/standalone-chrome` with 

```
docker run --rm -p 4444:4444 --shm-size=2g selenium/standalone-chrome
```

Within a `python` runtime

```
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from bs4 import BeautifulSoup

driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub',desired_capabilities=DesiredCapabilities.CHROME)

driver.get("https://edition.cnn.com/")
``` 

Check if indeed CNN is loaded by going to

http://127.0.0.1:4444/wd/hub

And click 'Take Screenshot' within corresponding session

### Challenge 2 - Use Chrome Inspect to learn how the page is buildup

Every Website is made from `HTML`, `CSS`, and `JavaScript`. 
`HTML` consist of elements
```
<tag attribute="value">element content</tag>
```

The attributes `class` and `id` are frequently used for reference. `CSS` uses these references to attach style to them, this is referred to as a `CSS Selector`.

```
<element class="newsline" id="news_item_04" attribute="value">element content</element>
```

Read more about CSS Selectors 

https://en.wikipedia.org/wiki/Cascading_Style_Sheets#Selector

Within a other Chrome/Firefox window, we can use `right click` > `inspect` to read out how the web developer names all the elements, and which `CSS Selectors` are used i.e.

```
<span class="cd__headline-text">Horror of Afghanistan brought to Hollywood</span>
```

Selenium provides the following methods to locate elements in a page:

```
find_element_by_id()
find_element_by_name()
find_element_by_xpath()
find_element_by_link_text()
find_element_by_partial_link_text()
find_element_by_tag_name()
find_element_by_class_name()
find_element_by_css_selector()
```

To find multiple elements (these methods will return a list):
```
find_elements_by_name()
find_elements_by_xpath()
find_elements_by_link_text()
find_elements_by_partial_link_text()
find_elements_by_tag_name()
find_elements_by_class_name()
find_elements_by_css_selector()
```
https://selenium-python.readthedocs.io/locating-elements.html

What `class`, `tag`, or other thing do the headlines have in common?

### Challenge 3 - Print all Raw HTML of all the News Title (with a for loop)



```
headlines = driver.find_elements_by_class_name("cd__headline-text")

for headline in headlines:
	raw_html = headline.get_attribute('outerHTML')
	print( raw_html )
```

### Challenge 4 - Use BS4 to parse the HTML of all the News Title (with a for loop)

```
from bs4 import BeautifulSoup

headlines = driver.find_elements_by_class_name("cd__headline-text")

for headline in headlines:
	raw_html = headline.get_attribute('outerHTML')
	soup = BeautifulSoup(raw_html)
	print( soup.text )

```

`BeautifulSoup` can unrafel any `html` page, and is usually used to do more advanced text or element fetching.


### What we didn't do today...

- After you found a element, say a `button` with `selenium`, you can `button.click()` to open that page
- After you found a element, say a search `field`, you can type text with `field.send_keys( "abc" )`
- The `driver` has a lot of options, like `driver.set_window_size(1280,1280)`, `driver.maximize_window()`, and `driver.implicitly_wait(2)`
- https://selenium-python.readthedocs.io/waits.html#
- *"We suspect that you are a Robot"* - Oh Nooo!

---

XPath Finder for Chrome
https://chrome.google.com/webstore/detail/xpath-finder/ihnknokegkbpmofmafnkoadfjkhlogph?hl=en

Use in combination with
`find_element_by_xpath()` or `find_elements_by_xpath()`
